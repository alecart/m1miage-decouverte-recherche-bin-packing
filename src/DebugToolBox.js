import { randomColorHexa } from "./ToolBox.js";

function getDebugSizes() {
    var res = [
        {
            color: randomColorHexa(),
            length: 12,
            width: 19
        },
        {
            color: randomColorHexa(),
            length: 12,
            width: 19
        },

        {
            color: randomColorHexa(),
            length: 7,
            width: 19
        },
        {
            color: randomColorHexa(),
            length: 7,
            width: 19
        },

        {
            color: randomColorHexa(),
            length: 7,
            width: 14
        },
        {
            color: randomColorHexa(),
            length: 7,
            width: 14
        },
        {
            color: randomColorHexa(),
            length: 7,
            width: 14
        },

        {
            color: randomColorHexa(),
            length: 14,
            width: 10
        },
        {
            color: randomColorHexa(),
            length: 14,
            width: 10
        },
        {
            color: randomColorHexa(),
            length: 14,
            width: 10
        },
        {
            color: randomColorHexa(),
            length: 14,
            width: 10
        },
        {
            color: randomColorHexa(),
            length: 14,
            width: 10
        },

    ]

    return res
}
export { getDebugSizes };