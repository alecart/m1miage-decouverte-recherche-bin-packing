import { getUrlVars, getRandomSizes, sortPlusEnBasAGauche } from "../ToolBox.js";
import * as THREE from "../../node_modules/three/build/three.module.js";

var params = getUrlVars(window.location.search);
var boxes = getRandomSizes(Number(params.nbboites), Number(params.taillemin), Number(params.taillemax))


const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
);
camera.position.z = 40;
camera.position.y = 15;
camera.position.x = 30;

document.addEventListener("keydown", function (e) {

    if (e.keyCode === 37) {//gauche
        camera.position.x -= 0.5;
    }
    else if (e.keyCode === 39) {//droite
        camera.position.x += 0.5;
    }
    else if (e.keyCode === 38) {//haut
        camera.position.y += 0.5;
    }
    else if (e.keyCode === 40) {//base
        camera.position.y -= 0.5;
    }
    else if (e.keyCode === 65) {//devant
        camera.position.z += 0.5;
    }
    else if (e.keyCode === 69) {//derriere
        camera.position.z -= 0.1;
    }
    else if (e.keyCode === 90) {//gauche
        camera.rotation.x += 0.1;
    }
    else if (e.keyCode === 83) {//droite
        camera.rotation.x -= 0.1;
    }
    else if (e.keyCode === 81) {//haut
        camera.rotation.y += 0.1;
    }
    else if (e.keyCode === 68) {//base
        camera.rotation.y -= 0.1;
    }

});

const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
function render() {
    requestAnimationFrame(render);
    renderer.render(scene, camera);
}

const gridHelper = new THREE.GridHelper(100, 100);
gridHelper.rotation.x = Math.PI / 2;
scene.add(gridHelper);
render();

console.log("ALGO 2D")
const geometryBackground = new THREE.BoxGeometry(params.longueur, params.largeur, -0.01);
const materialBackground = new THREE.MeshBasicMaterial({ color: "#ffffff" });
const objetBackground = new THREE.Mesh(geometryBackground, materialBackground);
objetBackground.translateX(params.longueur / 2);
objetBackground.translateY(params.largeur / 2);
scene.add(objetBackground);

var points = [{ x: 0, y: 0, z: 0, width_max: Number(params.taillemax) }];
boxes.forEach((element) => {

    var point;
    for (let p_point of points) {
        if (p_point.x + element.length <= Number(params.longueur) && element.width <= p_point.width_max) {
            point = p_point;

            const index = points.indexOf(p_point);
            if (index > -1) {
                points.splice(index, 1);
            }
            break;
        }
    }

    placerBox(element, point)
    points = sortPlusEnBasAGauche(points)
});

function placerBox(element, point) {

    const geometry = new THREE.BoxGeometry(element.length, element.width, 0);
    const material = new THREE.MeshBasicMaterial({ color: element.color });
    const objet = new THREE.Mesh(geometry, material);

    objet.translateX(point.x + element.length / 2);
    objet.translateY(point.y + element.width / 2);
    objet.position.z = 0;

    scene.add(objet);

    //en haut à gauche de l'objet
    var x = point.x;
    var y = point.y + element.width;
    var z = 0;

    if (!points.some(p => p.y == y)) {

        var max_width = x == 0 ? element.width : (point.width_max - element.width)
        points.push({ x: x, y: y, z: z, width_max: max_width })

    }

    //en bas à droite de l'objet
    var x = point.x + element.length;
    var y = point.y;
    var z = 0;
    points.push({ x: x, y: y, z: z, width_max: element.width })
    render();
}