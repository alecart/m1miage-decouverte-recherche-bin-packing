var MaxSizeDefault = 10;
var MinSizeDefault = 1;

function randomIntFromInterval(p_min, p_max) { // min and max included 
    return Math.floor(Math.random() * (p_max - p_min + 1) + p_min);
}

function randomColorHexa() {
    return '#' + Math.random().toString(16).slice(2, 8).toUpperCase();
}

function getRandomSizes(p_nb = 10, p_taille_min = MinSizeDefault, p_taille_max = MaxSizeDefault, p_dimensions) {

    var res = p_dimensions == "2D" ? getRandomSizes2D(p_nb, p_taille_min, p_taille_max) : getRandomSizes3D(p_nb, p_taille_min, p_taille_max)
    return res;
}

function getRandomSizes2D(p_nb = 10, p_taille_min = MinSizeDefault, p_taille_max = MaxSizeDefault) {

    var res = [];
    for (let index = 0; index < p_nb; index++) {
        res.push({
            color: randomColorHexa(),
            length: randomIntFromInterval(p_taille_min, p_taille_max),
            width: randomIntFromInterval(p_taille_min, p_taille_max),
        });
    }
    return sort2DBox(res);
}

function getRandomSizes3D(p_nb = 10, p_taille_min = MinSizeDefault, p_taille_max = MaxSizeDefault) {

    var res = [];
    for (let index = 0; index < p_nb; index++) {
        res.push({
            color: randomColorHexa(),
            length: randomIntFromInterval(p_taille_min, p_taille_max),
            width: randomIntFromInterval(p_taille_min, p_taille_max),
            depth: randomIntFromInterval(p_taille_min, p_taille_max),
        })
    }
    return sort3DBox(res);
}

function sort2DBox(box) {
    return box.sort(function (a, b) {
        if (a.width < b.width) return 1
        if (a.width > b.width) return -1
        if (a.length < b.length) return 1
        if (a.length > b.length) return -1
        return 0
    })
}

function sort3DBox(box) {
    return box.sort(function (a, b) {
        if (a.depth < b.depth) return 1
        if (a.depth > b.depth) return -1
        if (a.width < b.width) return 1
        if (a.width > b.width) return -1
        if (a.length < b.length) return 1
        if (a.length > b.length) return -1
        return 0
    })
}

function sortPlusEnBasAGauche(point) {
    var tab = point.sort(function (a, b) {
        if (a.y < b.y) return -1
        if (a.y > b.y) return 1
        if (a.x < b.x) return -1
        if (a.x > b.x) return 1
        return 0
    })
    return tab

}

function getUrlVars(querystring) {
    for (var hash, vars = [], hashes = querystring.slice(querystring.indexOf("?") + 1).split("&"), i = 0; i < hashes.length; i++)
        hash = hashes[i].split("="),
            vars.push(hash[0]),
            vars[hash[0]] = hash[1];
    return vars
}

function plusEnBasAGauche(points) {
    return sortPlusEnBasAGauche(points)[0]
}

export { randomColorHexa, randomIntFromInterval, getRandomSizes, getRandomSizes2D, getRandomSizes3D, getUrlVars, plusEnBasAGauche, sortPlusEnBasAGauche };