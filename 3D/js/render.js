var hauteurBande = 20;
var largeurBande = 20;
var wireframe = false;
var profondeurBande = 30;
var taille = 600;
var inBande = new Array(hauteurBande);
var aPos = new Array(hauteurBande);
var aPosHauteur = new Array(hauteurBande);
var color = 0;

initBande();

$('#wf').on('click', function () {
	wireframe = $('#wf').is(':checked');
});

$('#hauteur, #profondeur, #largeur').on('click', function () {
	$(this).focus();
});

$('#creer').on('click', function () {
	var nbPiece = parseInt($('#nbPieces').val());

	var mesObjets = [];
	for (var i = 0; i < nbPiece; i++) {

		var h = Math.floor((8 - 1) * Math.random());
		var l = Math.floor((8 - 1) * Math.random());
		var p = Math.floor((8 - 1) * Math.random());

		if (h == 0) h = 1;
		if (l == 0) l = 1;
		if (p == 0) p = 1;

		var unObjet = {};
		unObjet.h = h;
		unObjet.l = l;
		unObjet.p = p;
		mesObjets.push(unObjet);
	};

	mesObjets.sort(function (a, b) {
		return a.l - b.l;
	});
	mesObjets.reverse();

	for (var k in mesObjets) {
		unObjet = mesObjets[k];
		dessinerCube(unObjet.h, unObjet.l, unObjet.p);
	}
});

$('#vider').on('click', function () {
	location.reload();
});

var bande;
var camera, scene, renderer;
var projector, plane;
var mouse2D, mouse3D, raycaster, camPos = 45, camPos2 = 45,
	isFlecheGauche = false, isFlecheDroite = false, isFlecheHaut = false, isFlecheBas = false,
	vectors = new THREE.Vector3(0, 200, 0);

init();
animate();

function infos() {

	var perte = 0;
	var utilise = 0;
	var max = 0
	for (var i = 0; i < hauteurBande; i++) {
		for (var j = 0; j < largeurBande; j++) {
			for (var k = 0; k < profondeurBande; k++) {
				if (inBande[i][j][k] == 1) {
					if (i > max) {
						max = i;
					}
				}
			}
		}
	}
	max++;
	var total = max * largeurBande;
	for (var i = 0; i < max; i++) {
		for (var j = 0; j < largeurBande; j++) {
			for (var k = 0; k < profondeurBande; k++) {
				if (inBande[i][j][k] == 0) {
					perte++;
				} else {
					utilise++;
				}
			}
		}
	}
	var perteTotal = ((perte / total) * 100 / profondeurBande);
	var utiliseTotal = ((utilise / total) * 100 / profondeurBande);
	$('#perte').html(perteTotal.toFixed(2) + ' %');
	$('#utilise').html(utiliseTotal.toFixed(2) + ' %');
}

function init() {
	bande = document.createElement('div');
	$("#container").append(bande);
	camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 10000);
	camera.position.y = 800;
	scene = new THREE.Scene();
	var size = 1000;
	espace = 50;
	tmpEspace = 0;
	var geometry = new THREE.Geometry();
	for (var i = - size / 2; i <= size; i += espace) {
		geometry.vertices.push(new THREE.Vector3(- size / 2, 0, i));
		geometry.vertices.push(new THREE.Vector3(size / 2, 0, i));
	}
	for (var i = - size / 2; i <= size / 2; i += espace) {

		geometry.vertices.push(new THREE.Vector3(i, 0, - size / 2));
		geometry.vertices.push(new THREE.Vector3(i, 0, size));

	}
	var material = new THREE.LineBasicMaterial({ color: 0x000000, opacity: 0.2 });
	var line = new THREE.Line(geometry, material);
	line.type = THREE.LinePieces;
	scene.add(line);
	projector = new THREE.Projector();
	plane = new THREE.Mesh(new THREE.PlaneGeometry(1000, 1000), new THREE.MeshBasicMaterial());
	plane.rotation.x = - Math.PI / 2;
	plane.visible = false;
	scene.add(plane);

	mouse2D = new THREE.Vector3(0, 10000, 0.5);

	var lumiere = new THREE.AmbientLight(0xffffff);
	scene.add(lumiere);

	renderer = new THREE.CanvasRenderer();
	renderer.setSize(window.innerWidth, window.innerHeight);

	bande.appendChild(renderer.domElement);

	document.addEventListener('mousemove', onDocumentMouseMove, false);
	document.addEventListener('keydown', onDocumentKeyDown, false);
	document.addEventListener('keyup', onDocumentKeyUp, false);

}

function onDocumentMouseMove(event) {
	event.preventDefault();
	mouse2D.x = (event.clientX / window.innerWidth) * 2 - 1;
	mouse2D.y = - (event.clientY / window.innerHeight) * 2 + 1;
}


function dessinerCube(hauteur, largeur, profondeur) {
	var geometry = new THREE.CubeGeometry(largeur * 50, hauteur * 50, profondeur * 50);
	var couleur = "";
	switch (color) {
		case 0:
			couleur = '0xFF0000'; color = 1; break;
		case 1: couleur = '0x00FF00'; color = 2; break;
		case 2: couleur = '0x0000FF'; color = 0; break;
	}

	for (var i = 0; i < geometry.faces.length; i++) {
		geometry.faces[i].color.setHex(couleur);
	}
	var material = new THREE.MeshLambertMaterial({ vertexColors: THREE.FaceColors, wireframe: wireframe });
	var cube = new THREE.Mesh(geometry, material);
	var pos = getEmplacement(hauteur, largeur, profondeur);
	cube.position.x = pos.y * 50 + (50 * largeur) / 2 - 500;
	cube.position.y = pos.x * 50 + (50 * hauteur) / 2;
	cube.position.z = pos.z * 50 + (50 * profondeur) / 2 - 500;
	cube.matrixAutoUpdate = false;
	cube.updateMatrix();
	scene.add(cube);
	infos();
}

function getEmplacement(h, l, p) {

	var ok = false;
	var posOK = false;
	var fok = true;
	var X = 0;
	var Y = 0;
	var Z = 0;
	for (var i = 0; i < hauteurBande; i++) {
		for (var j = 0; j < largeurBande; j++) {
			for (var k = 0; k < profondeurBande; k++) {
				if (inBande[i][j][k] == 0) {
					for (var ii = i; ii < (parseInt(h) + i); ii++) {
						for (var jj = j; jj < (parseInt(l) + j); jj++) {
							for (var mm = k; mm < (parseInt(p) + k); mm++) {
								if (jj > hauteurBande || ii > largeurBande || mm > profondeurBande || inBande[ii][jj][mm] != 0) {
									if (ok) fok = false;
									ok = false;
									break;
								} else {

									if (fok) ok = true;
								}
							}
						}
						if (!fok) {
							fok = true;
							break;
						}
					}
					if (ok == true) {
						X = i;
						Y = j;
						Z = k;
						for (var iii = i; iii < (parseInt(h) + i); iii++) {
							for (var jjj = j; jjj < (parseInt(l) + j); jjj++) {
								for (var kkk = k; kkk < (parseInt(p) + k); kkk++) {
									inBande[iii][jjj][kkk] = 1;
								}
							}
						}
						posOK = true;
					}
				}
				if (posOK) {
					break;
				}
			}
			if (posOK) {
				break;
			}
		}
		if (posOK) {
			break;
		}
	}
	if (posOK) {
		return { x: X, y: Y, z: Z }
	} else {
		return false;
	}
}
function onDocumentKeyDown(event) {
	switch (event.keyCode) {
		case 37: isFlecheGauche = true; break;
		case 38: isFlecheHaut = true; break;
		case 39: isFlecheDroite = true; break;
		case 40: isFlecheBas = true; break;
		case 13: $('#creer').click(); break;
	}
}

function onDocumentKeyUp(event) {
	switch (event.keyCode) {
		case 37: isFlecheGauche = false; break;
		case 38: isFlecheHaut = false; break;
		case 39: isFlecheDroite = false; break;
		case 40: isFlecheBas = false; break;
	}
}


function animate() {
	requestAnimationFrame(animate);
	render();
}

function render() {

	if (isFlecheGauche) {
		camPos += mouse2D.x * -3;
	}
	if (isFlecheDroite) {
		camPos += mouse2D.x * 3;
	}
	if (isFlecheHaut) {
		camPos2 += mouse2D.y * 3;
	}
	if (isFlecheBas) {
		camPos2 += mouse2D.y * -3;
	}

	camera.position.x = 2500 * Math.sin(camPos * Math.PI / 360);
	camera.position.y = 2500 * Math.sin(camPos2 * Math.PI / 360);
	camera.position.z = 2500 * Math.cos(camPos * Math.PI / 360);
	camera.lookAt(vectors);

	raycaster = projector.pickingRay(mouse2D.clone(), camera);

	renderer.render(scene, camera);

}
function initBande() {
	for (var i = 0; i < hauteurBande; i++) {
		inBande[i] = new Array(largeurBande);
		for (var j = 0; j < profondeurBande; j++) {
			inBande[i][j] = new Array(profondeurBande);
			for (var k = 0; k < profondeurBande; k++) {
				inBande[i][j][k] = 0;
			}
		}
	}
	for (var l = 0; l < hauteurBande; l++) {
		aPos[l] = taille;
		taille -= 50;
	}
	var tailleHauteur = 25;
	for (var l = 0; l < hauteurBande; l++) {
		aPosHauteur[l] = tailleHauteur;
		tailleHauteur += 50;
	}
}

function getPositionXorZ(pos) {
	return aPos[pos];
}

function getPositionY(pos) {
	return aPosHauteur[pos];
}